object fSettings: TfSettings
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'fSettings'
  ClientHeight = 229
  ClientWidth = 447
  Color = clBtnFace
  ParentFont = True
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object TabControl: TTabControl
    Left = 0
    Top = 0
    Width = 447
    Height = 193
    Align = alTop
    DoubleBuffered = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    MultiLine = True
    ParentDoubleBuffered = False
    ParentFont = False
    Style = tsFlatButtons
    TabOrder = 2
    Tabs.Strings = (
      'Main'
      'Screenshot'
      'Calculation')
    TabIndex = 0
    OnChange = TabControlChange
    object PanelMain: TPanel
      Left = 4
      Top = 27
      Width = 439
      Height = 162
      Align = alClient
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object lbl1: TLabel
        Left = 119
        Top = 134
        Width = 50
        Height = 13
        Caption = 'Text Color'
      end
      object eSweatNotifiTime: TLabeledEdit
        Left = 211
        Top = 91
        Width = 49
        Height = 21
        Color = clBtnFace
        EditLabel.Width = 196
        EditLabel.Height = 13
        EditLabel.Caption = 'Last sweat notification shows for .. (sec)'
        LabelPosition = lpLeft
        NumbersOnly = True
        ParentShowHint = False
        ShowHint = False
        TabOrder = 0
      end
      object chkShowSweatInfo: TCheckBox
        Left = 17
        Top = 74
        Width = 202
        Height = 17
        Caption = 'Show info for sweat gatherers'
        TabOrder = 1
      end
      object eAskPeriod: TLabeledEdit
        Left = 211
        Top = 53
        Width = 49
        Height = 21
        Color = clBtnFace
        EditLabel.Width = 194
        EditLabel.Height = 13
        EditLabel.Caption = 'Period of re-reading info from file (msec)'
        LabelPosition = lpLeft
        NumbersOnly = True
        ParentShowHint = False
        ShowHint = False
        TabOrder = 2
      end
      object bOpen: TButton
        Left = 232
        Top = 20
        Width = 31
        Height = 31
        Hint = 'open log-file'
        HotImageIndex = 17
        ImageIndex = 16
        Images = fMain.il1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = bOpenClick
      end
      object ePath: TLabeledEdit
        Left = 7
        Top = 25
        Width = 221
        Height = 21
        Color = clBtnFace
        EditLabel.Width = 70
        EditLabel.Height = 13
        EditLabel.Caption = 'Path to log-file'
        ReadOnly = True
        TabOrder = 4
      end
      object clrbxText: TColorBox
        Left = 175
        Top = 131
        Width = 85
        Height = 22
        TabOrder = 5
      end
      object chkEditFormMode: TCheckBox
        Left = 16
        Top = 114
        Width = 97
        Height = 17
        Caption = 'Edit Mode'
        TabOrder = 6
      end
    end
    object PanelSS: TPanel
      Left = 4
      Top = 27
      Width = 439
      Height = 162
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 1
      Visible = False
      object Label2: TLabel
        Left = 74
        Top = 108
        Width = 84
        Height = 13
        Caption = 'Manual SS button'
        Enabled = False
        Visible = False
      end
      object eScrDelay: TLabeledEdit
        Left = 7
        Top = 106
        Width = 60
        Height = 21
        Hint = 'How long wait before take screenshot'
        EditLabel.Width = 41
        EditLabel.Height = 13
        EditLabel.Caption = 'SS delay'
        NumbersOnly = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object eAvName: TLabeledEdit
        Left = 7
        Top = 67
        Width = 190
        Height = 21
        Hint = 'keep it clear if you dont want to take autoscreens'
        EditLabel.Width = 165
        EditLabel.Height = 13
        EditLabel.Caption = 'Auto Screen for (full avatar name)'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object bOpenPath: TButton
        Left = 234
        Top = 20
        Width = 31
        Height = 31
        Hint = 'change path for saving screenshots'
        HotImageIndex = 17
        ImageIndex = 16
        Images = fMain.il1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = bOpenPathClick
      end
      object eScreenPath: TLabeledEdit
        Left = 7
        Top = 25
        Width = 222
        Height = 21
        Color = clBtnFace
        EditLabel.Width = 122
        EditLabel.Height = 13
        EditLabel.Caption = 'Path to save screenshots'
        ReadOnly = True
        TabOrder = 3
      end
      object SSHotKey: THotKey
        Left = 160
        Top = 106
        Width = 100
        Height = 19
        Enabled = False
        HotKey = 0
        InvalidKeys = []
        Modifiers = [hkExt]
        TabOrder = 4
        Visible = False
      end
    end
    object PanelCalc: TPanel
      Left = 4
      Top = 27
      Width = 439
      Height = 162
      Align = alClient
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      Visible = False
      object Label1: TLabel
        Left = 14
        Top = 4
        Width = 111
        Height = 26
        Caption = 'Preferred "RUN COST" calculation method'
        WordWrap = True
      end
      object rb1: TRadioButton
        Left = 9
        Top = 44
        Width = 24
        Height = 17
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TabStop = True
      end
      object rb2: TRadioButton
        Left = 9
        Top = 77
        Width = 24
        Height = 17
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object eCost: TLabeledEdit
        Left = 32
        Top = 42
        Width = 45
        Height = 21
        EditLabel.Width = 28
        EditLabel.Height = 13
        EditLabel.Caption = 'eCost'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        LabelPosition = lpRight
        LabelSpacing = 6
        ParentFont = False
        TabOrder = 2
        OnChange = eCostChange
        OnKeyPress = eCostKeyPress
      end
      object eEco: TLabeledEdit
        Left = 32
        Top = 75
        Width = 45
        Height = 21
        EditLabel.Width = 28
        EditLabel.Height = 13
        EditLabel.Caption = 'eCost'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        LabelPosition = lpRight
        LabelSpacing = 6
        ParentFont = False
        TabOrder = 3
      end
      object dbGrid: TDBGrid
        AlignWithMargins = True
        Left = 157
        Top = 4
        Width = 278
        Height = 154
        Align = alRight
        DataSource = dsWeapon
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        PopupMenu = pmWeaponList
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'ShotCost'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Description'
            Width = 185
            Visible = True
          end>
      end
    end
  end
  object bApply: TButton
    Left = 148
    Top = 199
    Width = 75
    Height = 25
    Caption = 'Apply'
    TabOrder = 1
    OnClick = bApplyClick
  end
  object bCancel: TButton
    Left = 59
    Top = 199
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 0
    OnClick = bCancelClick
  end
  object OpenDialog: TOpenDialog
    Left = 16
    Top = 184
  end
  object dsWeapon: TDataSource
    DataSet = fMain.mtWeapon
    Left = 388
    Top = 120
  end
  object pmWeaponList: TPopupMenu
    Left = 388
    Top = 72
    object UseCurrent1: TMenuItem
      Caption = 'Use Current'
      OnClick = UseCurrent1Click
    end
    object DeleteCurrent1: TMenuItem
      Caption = 'Delete Current'
      OnClick = DeleteCurrent1Click
    end
  end
end
