program EuDmg;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {fMain},
  Vcl.Themes,
  Vcl.Styles,
  uSettings in 'uSettings.pas' {fSettings},
  uDetailed in 'uDetailed.pas' {frmDetailed};

{$R *.res}

begin
  Application.Initialize;
  ReportMemoryLeaksOnShutdown := True;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'EUDmg';
  Application.CreateForm(TfMain, fMain);
  Application.CreateForm(TfSettings, fSettings);
  Application.CreateForm(TfrmDetailed, frmDetailed);
  Application.Run;
end.
