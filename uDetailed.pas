unit uDetailed;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, Vcl.ExtCtrls,
  Clipbrd, Vcl.Grids,
  Vcl.DBGrids, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, ShellApi;

type
  TfrmDetailed = class(TForm)
    gridLootedItems: TDBGrid;
    dsLoot: TDataSource;
    pTop: TPanel;
    lblVersion: TLabel;
    lblSumDmg: TLabel;
    lblHA: TLabel;
    lblCrit: TLabel;
    lblEvades: TLabel;
    lblMisses: TLabel;
    lblShots: TLabel;
    lblDpec: TLabel;
    btnLnk: TButton;
    PopupMenuRemoveLoot: TPopupMenu;
    RemoveLootLine1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure RefreshElements;
    procedure FormResize(Sender: TObject);
    procedure btnLnkClick(Sender: TObject);
    procedure RemoveLootLine1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDetailed: TfrmDetailed;

implementation

{$R *.dfm}

uses uMain;

procedure TfrmDetailed.btnLnkClick(Sender: TObject);
begin
  ShellExecute(Handle, 'open',
    'https://drive.google.com/drive/folders/0B126ji_D3il-TnBXWGNESTNQQlk?resourcekey=0-_OYsu0PGhdXI4eCiEE4CSw&usp=sharing', nil,
    nil, SW_SHOW);
end;

procedure TfrmDetailed.FormCreate(Sender: TObject);
begin
  frmDetailed.Caption := 'Detailed Information';
  lblVersion.Caption := 'v' + Version + ' (' + VersionDate + ') by Kromas';

  frmDetailed.Font.Color := fMain.Font.Color;
  frmDetailed.Top := fMain.Sett.Detailed_Top;
  frmDetailed.Left := fMain.Sett.Detailed_Left;
  frmDetailed.Width := fMain.Sett.Detailed_Width;
  frmDetailed.ClientHeight := fMain.Sett.Detailed_ClientHeight;

  RefreshElements;
end;

procedure TfrmDetailed.FormResize(Sender: TObject);
begin
  fMain.Sett.Detailed_Top := frmDetailed.Top;
  fMain.Sett.Detailed_Left := frmDetailed.Left;
  fMain.Sett.Detailed_Width := frmDetailed.Width;
  fMain.Sett.Detailed_ClientHeight := frmDetailed.ClientHeight;
  gridLootedItems.Columns[0].Width:= Round(frmDetailed.Width * 0.5);
  gridLootedItems.Columns[1].Width:= Round(frmDetailed.Width * 0.2);
  gridLootedItems.Columns[2].Width:= Round(frmDetailed.Width * 0.2);
end;

procedure TfrmDetailed.RefreshElements;
var
  DmgPec: extended;
  countME: integer;
begin
  lblSumDmg.Caption := 'Dmg: ' + FormatFloat('0', fMain.Stat.Dmg);

  // dmg/pec
  if fMain.Sett.CalcMethod = 1 then
  begin
    if ((fMain.Stat.Dmg > 0) and (fMain.Sett.ShotCost > 0)) then
    begin
      DmgPec := (fMain.Stat.Dmg / fMain.Stat.Shots) / fMain.Sett.ShotCost;
      lblDpec.Caption := 'Eco: ' + FormatFloat('0.000', DmgPec);
    end
    else
      lblDpec.Caption := 'Eco: ';
  end
  else
    lblDpec.Caption := 'Eco: ' + FormatFloat('0.000', fMain.Sett.CasualEco);

  // other info
  countME := fMain.Stat.Misses + fMain.Stat.Evades;
  lblShots.Caption := 'Shots: ' + IntToStr(fMain.Stat.Shots);
  lblMisses.Caption := 'Misses: ' + IntToStr(fMain.Stat.Misses);
  lblEvades.Caption := 'Evades: ' + IntToStr(fMain.Stat.Evades);

  if fMain.Stat.Shots > 0 then
  begin
    if countME > 0 then
      lblHA.Caption := 'Hit: ' + FormatFloat('0.00',
        (((fMain.Stat.Shots - countME) / fMain.Stat.Shots) * 100)) + '%';
    if fMain.Stat.Crits > 0 then
      lblCrit.Caption := 'Crit: ' + FormatFloat('0.00',
        ((fMain.Stat.Crits / (fMain.Stat.Shots - countME)) * 100)) + '%';
  end
  else
  begin
    lblHA.Caption := 'Hit: 100%';
    lblCrit.Caption := 'Crit: 0%';
  end;
end;

procedure TfrmDetailed.RemoveLootLine1Click(Sender: TObject);
begin
  fmain.LootTable_Remove(fmain.mtLoot.FieldByName('ItemName').AsString);
end;

end.
