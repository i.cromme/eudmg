object frmDetailed: TfrmDetailed
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'frmDetailed'
  ClientHeight = 426
  ClientWidth = 459
  Color = clBlack
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clLime
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesigned
  Scaled = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 16
  object gridLootedItems: TDBGrid
    AlignWithMargins = True
    Left = 0
    Top = 145
    Width = 459
    Height = 281
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alClient
    Ctl3D = True
    DataSource = dsLoot
    DrawingStyle = gdsGradient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentColor = True
    ParentCtl3D = False
    ParentFont = False
    PopupMenu = PopupMenuRemoveLoot
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -16
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ItemName'
        Width = 227
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Amount'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Value'
        Width = 96
        Visible = True
      end>
  end
  object pTop: TPanel
    Left = 0
    Top = 0
    Width = 459
    Height = 145
    Align = alTop
    TabOrder = 1
    object lblVersion: TLabel
      Left = 1
      Top = 1
      Width = 457
      Height = 11
      Align = alTop
      Alignment = taRightJustify
      Caption = 'lblVersion'
      Color = clLime
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clLime
      Font.Height = -9
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      ExplicitLeft = 5
      ExplicitTop = 3
      ExplicitWidth = 40
    end
    object lblSumDmg: TLabel
      Left = 14
      Top = 29
      Width = 65
      Height = 16
      Caption = 'lblSumDmg'
    end
    object lblHA: TLabel
      Left = 14
      Top = 51
      Width = 29
      Height = 16
      Caption = 'lblHA'
    end
    object lblCrit: TLabel
      Left = 14
      Top = 73
      Width = 33
      Height = 16
      Caption = 'lblCrit'
    end
    object lblEvades: TLabel
      Left = 160
      Top = 51
      Width = 53
      Height = 16
      Caption = 'lblEvades'
    end
    object lblMisses: TLabel
      Left = 160
      Top = 73
      Width = 51
      Height = 16
      Caption = 'lblMisses'
    end
    object lblShots: TLabel
      Left = 160
      Top = 29
      Width = 45
      Height = 16
      Caption = 'lblShots'
    end
    object lblDpec: TLabel
      Left = 14
      Top = 95
      Width = 41
      Height = 16
      Caption = 'lblDpec'
    end
    object btnLnk: TButton
      Left = 256
      Top = 9
      Width = 25
      Height = 25
      Hint = 'open updates page'
      HotImageIndex = 9
      ImageAlignment = iaCenter
      ImageIndex = 8
      Images = fMain.il1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnLnkClick
    end
  end
  object dsLoot: TDataSource
    DataSet = fMain.mtLoot
    Left = 80
    Top = 240
  end
  object PopupMenuRemoveLoot: TPopupMenu
    Left = 208
    Top = 256
    object RemoveLootLine1: TMenuItem
      Caption = 'Remove Loot Line'
      OnClick = RemoveLootLine1Click
    end
  end
end
