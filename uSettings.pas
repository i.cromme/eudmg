unit uSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Vcl.ExtCtrls,
  ShlObj, ActiveX, Vcl.Tabs, Vcl.ComCtrls, Vcl.Menus, Vcl.Samples.Spin,
  Vcl.Grids, Vcl.ValEdit,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, Vcl.DBGrids,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.UITypes;

type
  TfSettings = class(TForm)
    bApply: TButton;
    bCancel: TButton;
    OpenDialog: TOpenDialog;
    TabControl: TTabControl;
    PanelMain: TPanel;
    PanelSS: TPanel;
    PanelCalc: TPanel;
    eSweatNotifiTime: TLabeledEdit;
    chkShowSweatInfo: TCheckBox;
    eAskPeriod: TLabeledEdit;
    bOpen: TButton;
    ePath: TLabeledEdit;
    eScrDelay: TLabeledEdit;
    eAvName: TLabeledEdit;
    bOpenPath: TButton;
    eScreenPath: TLabeledEdit;
    Label1: TLabel;
    rb1: TRadioButton;
    rb2: TRadioButton;
    eCost: TLabeledEdit;
    eEco: TLabeledEdit;
    SSHotKey: THotKey;
    Label2: TLabel;
    clrbxText: TColorBox;
    lbl1: TLabel;
    chkEditFormMode: TCheckBox;
    dsWeapon: TDataSource;
    dbGrid: TDBGrid;
    pmWeaponList: TPopupMenu;
    UseCurrent1: TMenuItem;
    DeleteCurrent1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure bApplyClick(Sender: TObject);
    procedure bOpenPathClick(Sender: TObject);
    procedure bOpenClick(Sender: TObject);
    procedure TabControlChange(Sender: TObject);
    procedure eCostChange(Sender: TObject);
    procedure eCostKeyPress(Sender: TObject; var Key: Char);
    procedure DeleteCurrent1Click(Sender: TObject);
    procedure UseCurrent1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fSettings: TfSettings;

implementation

{$R *.dfm}

uses
  uMain, uDetailed;

function AdvBrowseDirectory(sCaption: string; wsRoot: WideString;
  var sDirectory: string; bEditBox: Boolean = False;
  bShowFiles: Boolean = False; bAllowCreateDirs: Boolean = True;
  bRootIsMyComp: Boolean = False): Boolean;

  function SelectDirCB(Wnd: HWND; uMsg: UINT; lParam, lpData: lParam)
    : Integer; stdcall;
  begin
    case uMsg of
      BFFM_INITIALIZED:
        SendMessage(Wnd, BFFM_SETSELECTION, Ord(True), Integer(lpData));
    end;
    Result := 0;
  end;

var
  WindowList: Pointer;
  BrowseInfo: TBrowseInfo;
  Buffer: PChar;
  RootItemIDList, ItemIDList: PItemIDList;
  ShellMalloc: IMalloc;
  IDesktopFolder: IShellFolder;
  Eaten, Flags: LongWord;
const
  BIF_USENEWUI = $0040;
  BIF_NOCREATEDIRS = $0200;
begin
  Result := False;
  if not DirectoryExists(sDirectory) then
    sDirectory := '';
  FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  begin
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      RootItemIDList := nil;
      if wsRoot <> '' then
      begin
        SHGetDesktopFolder(IDesktopFolder);
        IDesktopFolder.ParseDisplayName(Application.Handle, nil,
          POleStr(wsRoot), Eaten, RootItemIDList, Flags);
      end
      else
      begin
        if bRootIsMyComp then
          SHGetSpecialFolderLocation(0, CSIDL_DRIVES, RootItemIDList);
      end;
      OleInitialize(nil);
      with BrowseInfo do
      begin
        hwndOwner := Application.Handle;
        pidlRoot := RootItemIDList;
        pszDisplayName := Buffer;
        lpszTitle := PChar(sCaption);
        ulFlags := BIF_RETURNONLYFSDIRS or BIF_USENEWUI or BIF_EDITBOX *
          Ord(bEditBox) or BIF_BROWSEINCLUDEFILES * Ord(bShowFiles) or
          BIF_NOCREATEDIRS * Ord(not bAllowCreateDirs);
        lpfn := @SelectDirCB;
        if sDirectory <> '' then
          lParam := Integer(PChar(sDirectory));
      end;
      WindowList := DisableTaskWindows(0);
      try
        ItemIDList := ShBrowseForFolder(BrowseInfo);
      finally
        EnableTaskWindows(WindowList);
      end;
      Result := ItemIDList <> nil;
      if Result then
      begin
        ShGetPathFromIDList(ItemIDList, Buffer);
        ShellMalloc.Free(ItemIDList);
        sDirectory := Buffer;
      end;
    finally
      ShellMalloc.Free(Buffer);
    end;
  end;
end;

procedure TfSettings.bCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfSettings.bOpenClick(Sender: TObject);
begin
  if OpenDialog.Execute then
  begin
    fMain.Sett.LogFile_Path := OpenDialog.FileName;
    fMain.FileReopen;
  end;
end;

procedure TfSettings.bOpenPathClick(Sender: TObject);
var
  sSelectedDir: string;
begin
  if AdvBrowseDirectory('Choose path to Chat.log', '', sSelectedDir) then
  begin
    fMain.Sett.ScreenPath := sSelectedDir;
    eScreenPath.Text := fMain.Sett.ScreenPath;
  end;
end;

procedure TfSettings.DeleteCurrent1Click(Sender: TObject);
begin
  if fMain.mtWeapon.Active then
    fMain.mtWeapon.Delete;
end;

procedure TfSettings.eCostChange(Sender: TObject);
begin
  if (Pos('.', eCost.Text) = 1) then
    eCost.Text := '0' + eCost.Text
end;

procedure TfSettings.eCostKeyPress(Sender: TObject; var Key: Char);
begin
  if not((CharInSet(Key, NumbersSet)) or (Key = '.') or
    (Key = #13) or (Key = #08)) then
  begin
    ShowMessage('You can enter only "0..9" or "."');
    Key := #0;
  end;
  if ((Key = '.') and (Pos('.', eCost.Text) > 0)) then
    Key := #0;
end;

procedure TfSettings.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfSettings.FormCreate(Sender: TObject);
begin
  fSettings.Caption := 'Settings';
  eCost.EditLabel.Caption := 'ShotCost (pec)';
  eEco.EditLabel.Caption := 'Eco, dpp';
  ePath.Text := fMain.Sett.LogFile_Path;
  eAskPeriod.Text := IntToStr(fMain.Timer.Interval);
  if fMain.Sett.ShowSweatInfo <> 0 then
    chkShowSweatInfo.Checked := True
  else
    chkShowSweatInfo.Checked := False;
  if fMain.Sett.EditFormMode = 1 then
    chkEditFormMode.Checked := True
  else
    chkEditFormMode.Checked := False;
  eSweatNotifiTime.Text := IntToStr(fMain.Sett.SweatInfoNotifTime);
  eScreenPath.Text := fMain.Sett.ScreenPath;
  eAvName.Text := fMain.Sett.AvatarName;
  eScrDelay.Text := IntToStr(fMain.Sett.DelayBeforeSS);
  eCost.Text := FloatToStr(fMain.Sett.ShotCost, fMain.format_settings);
  eEco.Text := FloatToStr(fMain.Sett.CasualEco, fMain.format_settings);
  if fMain.Sett.CalcMethod = 1 then
    rb1.Checked := True
  else
    rb2.Checked := True;
  clrbxText.Selected := fMain.Font.Color;
  // load list of weapon presets
  if not(fMain.mtWeapon.Active) then
    fMain.mtWeapon.OpenOrExecute;
end;

procedure TfSettings.TabControlChange(Sender: TObject);
begin
  PanelMain.Hide;
  PanelSS.Hide;
  PanelCalc.Hide;
  case TabControl.TabIndex of
    0:
      PanelMain.Show;
    1:
      PanelSS.Show;
    2:
      PanelCalc.Show;
  end;
end;

procedure TfSettings.UseCurrent1Click(Sender: TObject);
begin
  if fMain.mtWeapon.Active then
    eCost.Text := FloatToStr(fMain.mtWeapon.FieldByName('ShotCost').AsFloat, fMain.format_settings);
end;

procedure TfSettings.bApplyClick(Sender: TObject);
begin
  fMain.Sett.ShotCost := 0;
  if eCost.Text <> '' then
  begin
    try
      eCost.Text := StringReplace(eCost.Text, ' ', '', [rfReplaceAll, rfIgnoreCase]);
      if not TryStrToFloat(eCost.Text, fMain.Sett.ShotCost, fMain.format_settings) then
      begin
        eCost.Text := StringReplace(eCost.Text, ',', '.', [rfReplaceAll, rfIgnoreCase]);
        fMain.Sett.ShotCost := StrToFloat(eCost.Text, fMain.format_settings);
      end;
    finally
    end;
  end;

  fMain.Sett.CasualEco := 0;
  if eEco.Text <> '' then
  begin
    try
      eEco.Text := StringReplace(eEco.Text, ' ', '', [rfReplaceAll, rfIgnoreCase]);
      if not TryStrToFloat(eEco.Text, fMain.Sett.CasualEco, fMain.format_settings) then
      begin
        eEco.Text := StringReplace(eEco.Text, ',', '.', [rfReplaceAll, rfIgnoreCase]);
        fMain.Sett.CasualEco := StrToFloat(eEco.Text, fMain.format_settings);
      end;
    finally
    end;
  end;

  if rb1.Checked then
    fMain.Sett.CalcMethod := 1
  else
    fMain.Sett.CalcMethod := 2;

  fMain.Timer.Interval := StrToInt(eAskPeriod.Text);

  if chkShowSweatInfo.Checked then
    fMain.Sett.ShowSweatInfo := 1
  else
    fMain.Sett.ShowSweatInfo := 0;

  if chkEditFormMode.Checked then
    fMain.Sett.EditFormMode := 1
  else
    fMain.Sett.EditFormMode := 0;

  fMain.Sett.SweatInfoNotifTime := StrToInt(eSweatNotifiTime.Text);
  fMain.Sett.AvatarName := eAvName.Text;
  fMain.Sett.DelayBeforeSS := StrToInt(eScrDelay.Text);

  fMain.Font.Color := clrbxText.Selected;
  frmDetailed.Font.Color := clrbxText.Selected;

  fMain.RefreshElements;
  frmDetailed.RefreshElements;

  if chkEditFormMode.Checked then
    MessageDlg('To exit from edit mode and save changes just Close Application.', mtInformation, [mbOk], 0);

  Close;
end;

end.
