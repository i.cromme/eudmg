﻿unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, IniFiles,
  DateUtils, Vcl.Imaging.JPEG, Vcl.ExtCtrls, Vcl.ImgList, Vcl.Buttons,
  Vcl.ComCtrls, Vcl.Menus, Clipbrd, StrUtils, FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  System.ImageList, FireDAC.Stan.StorageBin;

type
  TSettings = class
    ProgramDir: string;
    LogFile_Path: string;
    ScreenPath: string;
    AvatarName: string;
    ShotCost: extended;
    CasualEco: extended;
    ShowSweatInfo: integer;
    SweatInfoNotifTime: integer;
    DelayBeforeSS: integer;
    CalcMethod: integer;
    EditFormMode: integer;
    Detailed_Top: integer;
    Detailed_Left: integer;
    Detailed_Width: integer;
    Detailed_ClientHeight: integer;
  end;

  TStatistic = class
    Dmg: extended;
    Crits: integer;
    Shots: integer;
    Misses: integer;
    Evades: integer;
    Claims: integer;
    Sootos: integer;
    LootValue: extended;
    CostValue: extended;
    function ResultValue(): extended;
    procedure Recalc_CostValue(settings: TSettings);
  end;

  TfMain = class(TForm)
    Timer: TTimer;
    il1: TImageList;
    mtLoot: TFDMemTable;
    shpStatus: TShape;
    lblClaimsCount: TLabel;
    lblStatus: TLabel;
    lblRunCost: TLabel;
    lblRunRes: TLabel;
    lblReturn: TLabel;
    btnReset: TButton;
    btnSettings: TButton;
    btnInfoRun: TButton;
    btnExit: TButton;
    btnHide: TButton;
    mtWeapon: TFDMemTable;
    fltfldWeaponShotCost: TFloatField;
    strngfldWeaponDescription: TStringField;
    mtLootItemName: TStringField;
    mtLootAmount: TIntegerField;
    mtLootValue: TFloatField;
    procedure FileReopen;
    procedure Parse_LogLine(income_str: string);
    procedure RefreshElements;
    procedure MakeScreenShot(prefix: string; GlInfo: string);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer_FileRefresh(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnSettingsClick(Sender: TObject);
    procedure btnInfoRunClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnHideClick(Sender: TObject);
    procedure LootTable_Append(ItemName: string; ItemAmount: LargeInt; ItemValue: extended);
    procedure LootTable_Remove(ItemName: string);
    procedure mtWeaponBeforeInsert(DataSet: TDataSet);
    procedure LoadIniSettings;
    procedure SaveIniSettings;
  private
    { Private declarations }
  public
    { Public declarations }
    Sett: TSettings;
    Stat: TStatistic;
    format_settings:  TFormatSettings;
  end;

const
  NumbersSet = ['0' .. '9'];
  Version = '3.61';
  VersionDate = '2021 Dec 31';

var
  fMain: TfMain;
  f: TFileStream;
  StartPosition, EndPosition: Int64;
  FileStartMarked: Boolean;
  LastNoSweat: TdateTime;

implementation

{$R *.dfm}

uses
  uSettings, uDetailed;

procedure RamClean;
var
  MainHandle: THandle;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
  begin
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessID);
    SetProcessWorkingSetSize(MainHandle, DWORD(-1), DWORD(-1));
    CloseHandle(MainHandle);
  end;
end;

function CaptureWindow(const WindowHandle: HWnd): TBitmap;
var
  DC: HDC;
  wRect: TRect;
  Width, Height: integer;
begin
  if not IsWindow(WindowHandle) then
  begin
    Result := nil;
    Exit;
  end;
  // SW_HIDE, SW_SHOW, SW_SHOWNORMAL
  ShowWindow(WindowHandle, SW_SHOW);
  DC := GetWindowDC(WindowHandle);
  Result := TBitmap.Create;
  try
    GetWindowRect(WindowHandle, wRect);
    Width := wRect.Right - wRect.Left;
    Height := wRect.Bottom - wRect.Top;
    Result.Width := Width;
    Result.Height := Height;
    Result.Modified := True;
    BitBlt(Result.Canvas.Handle, 0, 0, Width, Height, DC, 0, 0, SRCCOPY);
  finally
    ReleaseDC(WindowHandle, DC);
  end;
end;

function FindNextWnd(StartHWND: HWnd; AString: string): HWnd;
var
  Buffer: array [0 .. 255] of Char;
begin
  Result := StartHWND;
  repeat
    Result := FindWindowEx(0, Result, nil, nil);
    GetWindowText(Result, Buffer, SizeOf(Buffer));
    if StrPos(StrUpper(Buffer), PChar(UpperCase(AString))) <> nil then
      Break;
  until (Result = 0);
end;

procedure TStatistic.Recalc_CostValue(settings: TSettings);
begin
  if settings.CalcMethod = 1 then
  begin
    if ((settings.ShotCost > 0) and (Shots > 0)) then
      CostValue := (Shots * settings.ShotCost) / 100
    else
      CostValue := 0;
  end
  else
  begin
    if ((settings.CasualEco > 0) and (Dmg > 0)) then
      CostValue := (Dmg / settings.CasualEco) / 100
    else
      CostValue := 0;
  end;
end;

function TStatistic.ResultValue(): extended;
begin
  Result := LootValue - CostValue;
end;

procedure TfMain.RefreshElements;
begin
  // show fields or not
  if Sett.ShowSweatInfo = 0 then
    shpStatus.Hide
  else
    shpStatus.Show;

  // edit mode on/off
  if Sett.EditFormMode = 1 then
    fMain.BorderStyle := bsSizeable
  else
    fMain.BorderStyle := bsNone;

  // info
  lblClaimsCount.Caption := 'Claims: ' + IntToStr(Stat.Claims) + '(' + IntToStr(Stat.Sootos) + ')';
  lblRunCost.Caption := 'Run cost: ' + FormatFloat('0.00', Stat.CostValue);

  if Stat.ResultValue > 0 then
  begin
    lblRunRes.Caption := 'Result: +' + FormatFloat('0.00', Stat.ResultValue);
    lblRunRes.Font.Color := clLime;
  end
  else
  begin
    lblRunRes.Caption := 'Result: ' + FormatFloat('0.00', Stat.ResultValue);
    lblRunRes.Font.Color := clRed;
  end;

  if Stat.CostValue > 0 then
    lblReturn.Caption := 'Return ' + FormatFloat('0', (Stat.LootValue / Stat.CostValue) * 100) + '%'
  else
    lblReturn.Caption := 'Return 100%';

  // sweat info show
  if Sett.ShowSweatInfo <> 0 then
  begin
    if SecondsBetween(LastNoSweat, Now) > Sett.SweatInfoNotifTime then
      shpStatus.Brush.Color := clGreen
    else
    begin
      shpStatus.Brush.Color := clRed;
      lblStatus.Caption := '  Can`t get sweat!';
    end;
  end;
end;

procedure TfMain.FormCreate(Sender: TObject);
begin
  fMain.Caption := 'DC v' + Version;
  lblStatus.Caption := 'tracking off';
  format_settings := TFormatSettings.Create();
  format_settings.DecimalSeparator:='.';
  Sett := TSettings.Create;
  Sett.ProgramDir := GetCurrentDir;
  Stat := TStatistic.Create();
  LoadIniSettings; // load settings
  LastNoSweat := IncHour(Now, -1);
  // try to start processing
  FileReopen;
  Timer.Enabled := True;
end;

procedure TfMain.FileReopen;
begin
  FileStartMarked := false;

  Stat.Dmg := 0;
  Stat.Claims := 0;
  Stat.Sootos := 0;
  Stat.Crits := 0;
  Stat.Shots := 0;
  Stat.Misses := 0;
  Stat.Evades := 0;
  Stat.LootValue := 0;
  if mtLoot.Active then
    mtLoot.EmptyView;

  try
    f := TFileStream.Create(Sett.LogFile_Path, fmOpenRead or fmShareDenyWrite);
    StartPosition := f.Size;
    FileStartMarked := True;
    FreeAndNil(f);
  except
    ShowMessage('Cannot open file ' + Sett.LogFile_Path);
  end;
end;

procedure TfMain.Timer_FileRefresh(Sender: TObject);
var
  c: Char;
  FileOpened: Boolean;
  str: string;
begin
  if FileStartMarked then // Open file
  begin
    lblStatus.Caption := 'tracking';
    FileOpened := false;
    try
      f := TFileStream.Create(Sett.LogFile_Path, fmOpenRead or fmShareDenyWrite);
      FileOpened := True;
    except
      FileOpened := false;
      lblStatus.Caption := 'log is not accessible';
    end;

    if FileOpened then // read data from file
    begin
      EndPosition := f.Size;
      f.Seek(StartPosition - 1, soFromBeginning);
      repeat
        f.Read(c, 1);
        StartPosition := f.Seek(0, soFromCurrent);
        if c = #10 then
        begin
          Parse_LogLine(str);
          str := '';
        end
        else
          str := str + c;
      until StartPosition = EndPosition;

      FreeAndNil(f); // Close file

    end;
    Stat.Recalc_CostValue(Sett);
    RefreshElements;
    frmDetailed.RefreshElements;
  end
  else
    lblStatus.Caption := 'tracking off';
end;

procedure TfMain.btnInfoRunClick(Sender: TObject);
begin
  if not frmDetailed.Visible then
    frmDetailed.Show;
end;

procedure TfMain.btnResetClick(Sender: TObject);
begin
  if FileStartMarked then
  begin
    try
      if MessageDlg('Clear calculated info?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        FileReopen;
    except
      ShowMessage('Cannot open file ' + Sett.LogFile_Path);
    end;
  end
  else
    ShowMessage('Tracking not started' + Sett.LogFile_Path);
end;

procedure TfMain.btnSettingsClick(Sender: TObject);
begin
  Application.CreateForm(TfSettings, fSettings);
  fSettings.ShowModal;
end;

procedure TfMain.btnHideClick(Sender: TObject);
begin
  fMain.WindowState := wsMinimized;
end;

procedure TfMain.Parse_LogLine(income_str: string);

  function CutFromStr(str, sub1, sub2: string): string;
  var
    st, fin: integer;
  begin
    // get part of str between sub1 and sub2
    Result := '';
    repeat
      st := Pos(sub1, str);
      if st > 0 then
      begin
        str := Copy(str, st + length(sub1), length(str) - 1);
        st := 1;
        fin := Pos(sub2, str);
        Result := Copy(str, st, fin - st);
        str := Copy(str, fin + length(sub2), length(str) - 1);
      end;
    until st <= 0;
    Result := Trim(Result);
  end;

  function GetStr_DmgValue(str: string): extended;
  var
    i, im: integer;
    str_in, str_res: string;
    c: Boolean;
  begin
    // get damage value from full string

    if (Pos('You inflicted', str) > 0)
        or (Pos('Critical hit - Additional damage! You inflicted ', str) > 0) then
      str_in := CutFromStr(str, 'You inflicted ', ' points of damage');

    str_res := '';
    c := false;
    im := length(str_in);
    i := 1;
    while ((c = false) and (i <= im)) do
      if CharInSet(str_in[i], ['0' .. '9', '.']) then
      begin
        str_res := str_res + str_in[i];
        i := i + 1;
      end
      else
        c := True;
    try
      Result := StrToFloat(str_res, format_settings);
    except
      Result := -1;
    end;
  end;

  function GetStr_GlobalInfo(str: string; Avatar_Name: string): string;
  var
    Mob_Name, Gl_Value: string;
    TypeGlobal: integer;
  begin
    // get global value from full string
    // TypeGlobal: 0 Hunt, 1 Mining, 2 Craft

    Mob_Name := CutFromStr(str, Avatar_Name + ' killed a creature (', ') with a value of');

    if Mob_Name <> '' then
      TypeGlobal := 0
    else
      Mob_Name := CutFromStr(str, Avatar_Name + ' found a deposit (', ') with a value of');

    if Mob_Name <> '' then
      TypeGlobal := 1
    else
    begin
      Mob_Name := CutFromStr(str, Avatar_Name + ' manufactured an item (', ') worth');
      if Mob_Name <> '' then
        TypeGlobal := 2;
    end;

    if Mob_Name <> '' then
      case TypeGlobal of
        0 or 1:
          Gl_Value := CutFromStr(str, ' with a value of ', ' PED');
        2:
          Gl_Value := CutFromStr(str, ' worth ', ' PED');
        else
          Gl_Value := '';
      end;

    try
      Result := Mob_Name + ' (' + Gl_Value + ' PED)';
    except
      Result := '';
    end;

  end;

  function GetStr_LootInfo(income_str: string; var Amount: largeint; var Value: extended): string;
  var
    ValueStr, AmountStr: string;
  begin
    // get Looted item name from full string
    Amount := 0;
    try
      AmountStr := CutFromStr(income_str, ' x (', ') Value: ');
      Amount := StrToInt(AmountStr);
    except
    end;

    Value := 0.0;
    try
      ValueStr := CutFromStr(income_str, ' Value: ', ' PED');
      Value := StrToFloat(ValueStr, format_settings);
    except
    end;

    try
      Result := CutFromStr(income_str, 'You received ', ' x (');
    except
      Result := 'Parsing Error!';
    end;
  end;

var
  PS, PG: integer;
  LootItem: string;
  LootAmount: largeint;
  LootValue: extended;
begin
  PS := Pos('[System] []', income_str);
  PG := Pos('[Globals] []', income_str);

  if ((PS > 0) and (PS < 30)) then // processing only system chat
  begin
    if (Pos('[] You inflicted', income_str) > 0) then
    begin
      if (GetStr_DmgValue(income_str) > -1) then
        Stat.Dmg := Stat.Dmg + GetStr_DmgValue(income_str);
      Stat.Shots := Stat.Shots + 1;
    end;

    if Pos('[] Critical hit - Additional damage! You inflicted', income_str) > 0
    then
    begin
      if (GetStr_DmgValue(income_str) > -1) then
        Stat.Dmg := Stat.Dmg + GetStr_DmgValue(income_str);
      Stat.Crits := Stat.Crits + 1;
      Stat.Shots := Stat.Shots + 1;
    end;

    if Pos('[] You missed', income_str) > 0 then
    begin
      Stat.Misses := Stat.Misses + 1;
      Stat.Shots := Stat.Shots + 1;
    end;

    if ((Pos('[] The target Evaded your attack', income_str) > 0) or
      (Pos('[] The target Dodged your attack', income_str) > 0) or
      (Pos('[] The target Jammed your attack', income_str) > 0)) then
    begin
      Stat.Evades := Stat.Evades + 1;
      Stat.Shots := Stat.Shots + 1;
    end;

    // sweat info calc
    if Sett.ShowSweatInfo <> 0 then
      if ((Pos('[] You may not acquire any more sweat from this creature',
        income_str) > 0) or (Pos('[] No target', income_str) > 0)) then
        LastNoSweat := Now;

    // mining info
    if (Pos('[] You have claimed a resource', income_str) > 0) then
    begin
      Stat.Claims := Stat.Claims + 1;
    end;

    if (Pos('[] You found something out of the ordinary', income_str) > 0) then
    begin
      Stat.Sootos := Stat.Sootos + 1;
    end;

    // loot info
    if (Pos('[] You received', income_str) > 0) then
    begin
      LootAmount := 0;
      LootValue := 0;
      LootItem := GetStr_LootInfo(income_str, LootAmount, LootValue);
      LootTable_Append(LootItem, LootAmount, LootValue);
      Stat.LootValue := Stat.LootValue + LootValue;
    end;
  end

  // string contains global info
  else if ((PG > 0) and (PG < 30) and (Sett.AvatarName <> '') and
    (Pos(Sett.AvatarName, income_str) > 0)) then
  begin
    MakeScreenShot('Auto', GetStr_GlobalInfo(income_str, Sett.AvatarName));
  end;
end;

procedure TfMain.MakeScreenShot(prefix: string; GlInfo: string);
var
  FileName: string;
  Capture: TBitmap;
  JPG: TJpegImage;
  EUWindowHandle: HWnd;
begin
  // check if path exists
  if DirectoryExists(Sett.ScreenPath) then
  begin
    if length(GlInfo) > 0 then
      GlInfo := '_' + GlInfo;
    FileName := Sett.ScreenPath + '\' + FormatDateTime('yyyy-mm-dd-hhmmss',
      Now()) + GlInfo + '.jpg';
    // sleep before auto-screening
    if prefix = 'Auto' then
      sleep(Sett.DelayBeforeSS);
    // add string to take screen for entropia window "Entropia Universe Client" only
    EUWindowHandle := FindNextWnd(0, 'Entropia Universe Client');
    if EUWindowHandle > 0 then
      Capture := CaptureWindow(EUWindowHandle)
    else // if EU not running then capture current window
      Capture := CaptureWindow(GetForegroundWindow);
    try
      JPG := TJpegImage.Create;
      JPG.Assign(Capture);
      JPG.SaveToFile(FileName);
    finally
      Capture.Free;
      JPG.Free;
    end;
  end
  else
    ShowMessage('Path for saving screens is not found');
end;

procedure TfMain.mtWeaponBeforeInsert(DataSet: TDataSet);
begin
  if DataSet.RecordCount >= 50 then
    Exit;
end;

procedure TfMain.LootTable_Append(ItemName: string; ItemAmount: LargeInt; ItemValue: extended);
var
  WasFound: Boolean;
begin
  WasFound := false;
  if not(mtLoot.Active) then
    mtLoot.OpenOrExecute;
  mtLoot.First;
  repeat
    if (mtLoot.FieldByName('ItemName').AsString = ItemName) then
    begin
      mtLoot.Edit;
      mtLoot.FieldByName('Amount').AsLargeInt := mtLoot.FieldByName('Amount').AsLargeInt + ItemAmount;
      mtLoot.FieldByName('Value').AsExtended := mtLoot.FieldByName('Value').AsExtended + ItemValue;
      mtLoot.Post;
      WasFound := True;
    end;
    mtLoot.Next;
  until (mtLoot.Eof or WasFound);

  if not WasFound then
  begin
    mtLoot.Append;
    mtLoot.FieldByName('ItemName').AsString := ItemName;
    mtLoot.FieldByName('Amount').AsLargeInt := ItemAmount;
    mtLoot.FieldByName('Value').AsExtended := ItemValue;
    mtLoot.Post;
  end;
end;

procedure TfMain.LootTable_Remove(ItemName: string);
var
  WasFound: Boolean;
begin
  WasFound := false;
  if not(mtLoot.Active) then
    mtLoot.OpenOrExecute;
  mtLoot.First;
  repeat
    if (mtLoot.FieldByName('ItemName').AsString = ItemName) then
    begin
      Stat.LootValue := Stat.LootValue - mtLoot.FieldByName('Value').AsExtended;
      mtLoot.Delete;
      WasFound:= True;
    end;
    mtLoot.Next;
  until (mtLoot.Eof or WasFound);
end;

procedure TfMain.LoadIniSettings;
var
  IniFile: TIniFile;
  eCostText, eCasualEcoText, s, ShotCost: string;
  i: integer;
begin
  IniFile := TIniFile.Create(Sett.ProgramDir + '\options.ini');

  fMain.Top := IniFile.ReadInteger('DESKTOP', 'Main_Top', fMain.Top);
  fMain.Left := IniFile.ReadInteger('DESKTOP', 'Main_Left', fMain.Left);
  fMain.Width := IniFile.ReadInteger('DESKTOP', 'Main_Width', fMain.Width);
  fMain.ClientHeight := IniFile.ReadInteger('DESKTOP', 'Main_ClientHeight', fMain.ClientHeight);

  Sett.Detailed_Top := IniFile.ReadInteger('DESKTOP', 'Detailed_Top', 0);
  Sett.Detailed_Left := IniFile.ReadInteger('DESKTOP', 'Detailed_Left', 10);
  Sett.Detailed_Width := IniFile.ReadInteger('DESKTOP', 'Detailed_Width', 250);
  Sett.Detailed_ClientHeight := IniFile.ReadInteger('DESKTOP', 'Detailed_ClientHeight', 220);

  Sett.LogFile_Path := IniFile.ReadString('OPTIONS', 'Path', '');
  Timer.Interval := IniFile.ReadInteger('OPTIONS', 'AskPeriod', 1000);
  eCostText := IniFile.ReadString('OPTIONS', 'LastShotCost', '20,0');
  eCasualEcoText := IniFile.ReadString('OPTIONS', 'CasualEco', '2,955');
  Sett.CalcMethod := IniFile.ReadInteger('OPTIONS', 'CalcMethod', 1);
  Sett.ShowSweatInfo := IniFile.ReadInteger('OPTIONS', 'ShowSweatInfo', 1);
  Sett.SweatInfoNotifTime := IniFile.ReadInteger('OPTIONS', 'SweatInfoNotificationTime', 10);
  Sett.ScreenPath := IniFile.ReadString('OPTIONS', 'Screenshot_Path', '');
  Sett.AvatarName := IniFile.ReadString('OPTIONS', 'Avatar_Name', '');
  Sett.DelayBeforeSS := IniFile.ReadInteger('OPTIONS', 'ScreenShotDelay', 1200);
  fMain.Font.Color := StrToInt(IniFile.ReadString('OPTIONS', 'TextColor', '$FFFFFF'));

  // Load Used Equipment List
  if not(mtWeapon.Active) then
    mtWeapon.OpenOrExecute;
  i := 1;
  repeat
    s := IniFile.ReadString('WEAPONS', 'Weapon_' + IntToStr(i), '');
    if s <> '' then
    begin
      mtWeapon.Append;
      mtWeapon.FieldByName('Description').AsString := Copy(s, Pos(']', s) + 1);
      ShotCost:= StringReplace(Copy(s, 2, Pos(']', s, 1) - 2), ',', '.', [rfReplaceAll, rfIgnoreCase]);
      mtWeapon.FieldByName('ShotCost').AsFloat := StrToFloat(ShotCost, format_settings);
      mtWeapon.Post;
    end;
    Inc(i);
  until (s = '') or (i > 500);

  if Assigned(IniFile) then
    FreeAndNil(IniFile);

  Sett.ShotCost := 0;
  if eCostText <> '' then
  begin
    if not TryStrToFloat(eCostText, Sett.ShotCost, format_settings) then
      Sett.ShotCost := 0;
  end;
  Sett.CasualEco := 0;
  if eCasualEcoText <> '' then
  begin
    if not TryStrToFloat(eCasualEcoText, Sett.CasualEco, format_settings) then
      Sett.CasualEco := 0;
  end;
end;

procedure TfMain.SaveIniSettings;
var
  IniFile: TIniFile;
begin
  IniFile := TIniFile.Create(Sett.ProgramDir + '\options.ini');
  IniFile.WriteInteger('DESKTOP', 'Main_Top', fMain.Top);
  IniFile.WriteInteger('DESKTOP', 'Main_Left', fMain.Left);
  IniFile.WriteInteger('DESKTOP', 'Main_Width', fMain.Width);
  IniFile.WriteInteger('DESKTOP', 'Main_ClientHeight', fMain.ClientHeight);
  IniFile.WriteInteger('DESKTOP', 'Detailed_Top', frmDetailed.Top);
  IniFile.WriteInteger('DESKTOP', 'Detailed_Left', frmDetailed.Left);
  IniFile.WriteInteger('DESKTOP', 'Detailed_Width', frmDetailed.Width);
  IniFile.WriteInteger('DESKTOP', 'Detailed_ClientHeight', frmDetailed.ClientHeight);
  IniFile.WriteString('OPTIONS', 'Path', Sett.LogFile_Path);
  IniFile.WriteInteger('OPTIONS', 'AskPeriod', Timer.Interval);
  IniFile.WriteString('OPTIONS', 'LastShotCost', FloatToStr(Sett.ShotCost, format_settings));
  IniFile.WriteString('OPTIONS', 'CasualEco', FloatToStr(Sett.CasualEco, format_settings));
  IniFile.WriteInteger('OPTIONS', 'CalcMethod', Sett.CalcMethod);
  IniFile.WriteInteger('OPTIONS', 'ShowSweatInfo', Sett.ShowSweatInfo);
  IniFile.WriteInteger('OPTIONS', 'SweatInfoNotificationTime',Sett.SweatInfoNotifTime);
  IniFile.WriteString('OPTIONS', 'Screenshot_Path', Sett.ScreenPath);
  IniFile.WriteString('OPTIONS', 'Avatar_Name', Sett.AvatarName);
  IniFile.WriteInteger('OPTIONS', 'ScreenShotDelay', Sett.DelayBeforeSS);
  IniFile.WriteString('OPTIONS', 'TextColor', Format('$%8.8x', [fMain.Font.Color]));

  // Save Used Equipment List
  IniFile.EraseSection('WEAPONS');
  mtWeapon.First;
  while not mtWeapon.Eof do
  begin
    IniFile.WriteString('WEAPONS', 'Weapon_' + IntToStr(mtWeapon.RecNo),
      '[' + FloatToStr(mtWeapon.FieldByName('ShotCost').AsFloat, format_settings) + ']' +
      mtWeapon.FieldByName('Description').AsString);
    mtWeapon.Next;
  end;

  if Assigned(IniFile) then
    FreeAndNil(IniFile);

end;

procedure TfMain.btnExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if MessageDlg('Close application?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
  then
    abort;
  Timer.Enabled := false;

  SaveIniSettings;

  RamClean;
  Action := caFree;

  if Assigned(Stat) then
    FreeAndNil(Stat);

  if Assigned(Sett) then
    FreeAndNil(Sett);
end;

end.
